const { range, randInt } = require("./Utils");
const chalk = require("chalk");
const Table = require("cli-table");

class Board {
    constructor(numCols, numRows, defaultChar, colSeporator) {
        this.numCols = numCols;
        this.numRows = numRows;
        this.defaultChar = defaultChar || ' ';
        this.colSeporator = colSeporator || '|';

        this.moveRange = new range(this.numCols);
        this.mainBoard = [];
        this.boardStatus = "";
        this.aiStats = "";

        this.init();
    }

    createBlankRow() {
        let blankRow = [];

        for (let i = 0; i < this.numCols; i++) {
            blankRow.push(this.defaultChar);
        }

        return blankRow;
    }

    init() {
        this.mainBoard = [];
        this.boardStatus = "Status: ";
        for (let i = 0; i < this.numRows; i++) {
            this.mainBoard.push(this.createBlankRow());
        }
    }

    print() {
        let rows = [];

        // if side header this should be [""] instead of []
        let head = [];
        
        for (let i = 0; i < this.numRows; i++) {
            let row = this.mainBoard[i];
            rows.push(row.join(this.colSeporator));
        }
        for (let i = 0; i < this.numCols; i++) {
            head.push(i);
        }

        console.clear();

        let table = new Table({
            head: head,
        });

        for (let i = 0; i < rows.length; i++) {
            let row = rows[i];
            row = row.replaceAll("X", chalk.redBright("@")).replaceAll("O", chalk.blueBright("@"));
            table.push(row.split(this.colSeporator));
            // table.push({ [i]: row.split(this.colSeporator) });
        }

        console.log(table.toString());
        console.log('\n' + this.aiStats);
        console.log(this.boardStatus);
    }

    randMove() {
        let move;
        while (true) {
            move = randInt(0, this.numRows);
            if (this.isValid(move)) {
                return move;
            }
        }
    }

    isValid(colIdx) {
        return (this.moveRange.includes(colIdx) && this.mainBoard[0][colIdx] === this.defaultChar);
    }

    nthcolumn(n) {
        const matrix = this.mainBoard;
        let column = [];
        for (let i = 0; i < matrix.length; i++) {
            let row = matrix[i];
            column.push(row[n]);
        }
        return column;
    }

    checkChar(rowIdx, colIdx) {
        return this.mainBoard[rowIdx][colIdx] !== this.defaultChar;
    }

    checkHorizontal(char, numInARow = 4) {
        for (let j = 0; j < this.numCols - numInARow; j++) {
            for (let i = 0; i < this.numRows; i++) {
                if (this.mainBoard[i][j] == char && this.mainBoard[i][j + 1] == char && this.mainBoard[i][j + 2] == char && this.mainBoard[i][j + 3] == char) {
                    return true;
                }
            }
        }
    }

    checkVertical(char, numInARow = 4) {
        for (let i = 0; i < this.numRows - numInARow; i++) {
            for (let j = 0; j < this.numCols; j++) {
                if (this.mainBoard[i][j] == char && this.mainBoard[i + 1][j] == char && this.mainBoard[i + 2][j] == char && this.mainBoard[i + 3][j] == char) {
                    return true;
                }
            }
        }
    }

    checkAscendingDiagonal(char, numInARow = 4) {
        for (let i = 3; i < this.numRows; i++) {
            for (let j = 0; j < this.numCols - numInARow; j++) {
                if (this.mainBoard[i][j] == char && this.mainBoard[i - 1][j + 1] == char && this.mainBoard[i - 2][j + 2] == char && this.mainBoard[i - 3][j + 3] == char)
                    return true;
            }
        }
    }

    checkDecendingDiagonal(char, numInARow = 4) {
        for (let i = 3; i < this.numRows; i++) {
            for (let j = 3; j < this.numCols; j++) {
                if (this.mainBoard[i][j] == char && this.mainBoard[i - 1][j - 1] == char && this.mainBoard[i - 2][j - 2] == char && this.mainBoard[i - 3][j - 3] == char)
                    return true;
            }
        }
    }

    checkDiagonal(char, numInARow = 4) {
        let accendingDiagonal = this.checkAscendingDiagonal(char, numInARow);
        let decendingDiagonal = this.checkDecendingDiagonal(char, numInARow);
        return accendingDiagonal || decendingDiagonal;
    }

    areFourConnected(char, numInARow = 4) {
        numInARow--;
        let horizontal = this.checkHorizontal(char, numInARow);
        let vertical = this.checkVertical(char, numInARow);
        let diagonal = this.checkDiagonal(char, numInARow);
        return horizontal || vertical || diagonal;
    }

    checkWin(char) {
        if (this.moveRange.every(x => !this.isValid(x))) return -1;
        if (this.areFourConnected(char)) return 1;
        return 0;
    }

    checkAllWins(playerChar, aiChar) {
        let isGameOver = false;
        let playerResult = this.checkWin(playerChar);
        let AIResult = this.checkWin(aiChar);

        if (playerResult === 1) {
            isGameOver = true;
            this.boardStatus = "Status: Player Won";
        }
        if (AIResult === 1) {
            isGameOver = true;
            this.boardStatus = "Status: AI Won";
        }
        if (playerResult === -1) {
            isGameOver = true;
            this.boardStatus = "Status: Tie";
        }

        return isGameOver;
    }

    setPiece(char, rowIdx, colIdx) {
        return this.mainBoard[rowIdx][colIdx] = char;
    }

    placePiece(char, colIdx) {
        let foundSpot = false;

        for (let rowIdx = 0; rowIdx < this.numRows; rowIdx++) {
            if (this.checkChar(rowIdx, colIdx)) {
                this.setPiece(char, rowIdx - 1, colIdx);
                foundSpot = true;
                break;
            }
        }

        if (!foundSpot) {
            this.setPiece(char, this.numRows - 1, colIdx);
        }
    }
}

module.exports = Board;