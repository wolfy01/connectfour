const Board = require("./Board.js");
const { input } = require("./Utils.js");

class Game {
    constructor(numCols, numRows, defaultChar, colSeporator) {
        this.numCols = numCols;
        this.numRows = numRows;
        this.board = new Board(numCols, numRows, defaultChar, colSeporator);
        this.over = false;

        this.playerChar = "O";
        this.aiChar = "X";
    }

    start() {
        let response;
        this.board.print();
        (async () => {
            while(!this.over) {
                await this.playerMove();
                this.over = this.board.checkAllWins(this.playerChar, this.aiChar);
                this.board.print();

                if (!this.over) {
                    console.log('-----AI TURN-----');
                    let startTime = performance.now();
                    await this.AIMove();
                    let endTime = performance.now();
                    this.board.aiStats = `AI Time: ${endTime - startTime} ms`;
                    this.over = this.board.checkAllWins(this.playerChar, this.aiChar);
                    this.board.print();
                }
                
                if (this.over) {
                    response = await input("Hit 'R' and enter to play again or just hit enter to quit: ");
                    if ((response.toUpperCase() === "R")) {
                        this.over = false;
                        this.board.init();
                        this.board.print();
                    } else {
                        process.exit();
                    }
                }
            }
        })();
    }

    playerMove() {
        return new Promise(async (resolve, reject) => {
            let move;

            while(true) {
                move = await input("Enter your column: ");

                if (move === "" || Number.parseInt(move) === NaN) {
                    console.log("Enter a number!");
                    continue;
                }

                move = Number.parseInt(move);

                if (this.board.isValid(move)) {
                    break;
                } else {
                    console.log("That move is invalid!");
                }
            }

            this.board.placePiece(this.playerChar, move);
            resolve();
        });
    }

    AIMove() {
        return new Promise(async (resolve, reject) => {
            let bestWins = null;
            let bestColumn = null;
        
            for (let column of this.board.moveRange) {
                if (this.board.isValid(column)) {
                    let aiWins = 0;
        
                    for (let i = 0; i < (this.numCols * this.numRows); i++) {
                        let testGameOver = 0;
                        let testBoard = new Board(this.board.numCols, this.board.numRows, this.board.defaultChar, this.board.colSeporator);
                        testBoard.mainBoard = JSON.parse(JSON.stringify(this.board.mainBoard));
                        let aiMove = column;
                        testBoard.placePiece(this.aiChar, aiMove);
                        testGameOver = testBoard.checkWin(this.aiChar);
                        if (testGameOver === 1) aiWins++;
        
                        while (testGameOver === 0) {
                            let plMove = testBoard.randMove();
                            testBoard.placePiece(this.playerChar, plMove);
                            testGameOver = testBoard.checkWin(this.playerChar);
        
                            if (testGameOver === 1) {
                                break;
                            }
        
                            aiMove = testBoard.randMove();
                            testBoard.placePiece(this.aiChar, aiMove);
                            testGameOver = testBoard.checkWin(this.aiChar);
        
                            if (testGameOver === 1) {
                                aiWins++;
                                break;
                            }
                        }
        
                        if (aiWins > bestWins) {
                            bestWins = aiWins;
                            bestColumn = column;
                        }
        
                        if (bestWins === null) {
                            for (let column in this.board.moveRange) {
                                if (this.board.isValid(column)) {
                                    bestColumn = column;
                                }
                            }
                        }
                    }
                }
            }
            this.board.placePiece(this.aiChar, bestColumn);
            resolve();
        });
    }
}

module.exports = Game;