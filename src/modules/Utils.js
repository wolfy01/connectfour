const readline = require('readline');
const rl = readline.createInterface({
    input: process.stdin,
    output: process.stdout
});

function input(prompt) {
    return new Promise((resolve, reject) => {
        console.log();
        rl.question(prompt, (response) => {
            resolve(response);
        });
    });
}

function randInt(min, max) {
    return (Math.random() * (max - min + 1)) << 0;
}

function range(size, startAt = 0) {
    return [...Array(size).keys()].map(i => i + startAt);
}

module.exports = {
    input,
    randInt,
    range
}